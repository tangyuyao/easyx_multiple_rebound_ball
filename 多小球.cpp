#include <conio.h>
#include <graphics.h>
#include <time.h>
#define number 10		//小球个数
struct Ball
{
	int ball_x;
	int ball_y;
	int ball_vx;
	int ball_vy;
	int distance[2];		//记录某个小球，距离它最近的小球的距离，以及这个小球的下标
};
Ball balls[number];

int main()
{
	void get_distance(struct Ball balls[number]);
	void knock(struct Ball balls[number]);
	//初始化所有小球位置
	srand((unsigned)time(NULL));
	for(int i=0;i<number;i++)
	{
		balls[i].ball_x=rand()%600+20;
		balls[i].ball_y=rand()%360+20;
				for(int j=0;j<number;j++)
			if(i!=j)		//不用和自己比
			{
				//初始化让小球不会轻易重叠
				if((balls[i].ball_x>=balls[j].ball_x-20)&&(balls[i].ball_x<=balls[j].ball_x+20)&&
					(balls[i].ball_y>=balls[j].ball_y-20)&&(balls[i].ball_y<=balls[j].ball_y+20))
				{
					balls[i].ball_x=rand()%600+20;
					balls[i].ball_y=rand()%360+20;
				}
			}
		balls[i].ball_vx=1;
		balls[i].ball_vy=1;
		//printf("[%d,%d]\n",balls[i].ball_x,balls[i].ball_y);
	}
	for (i=0;i<number;i++)
    {
        balls[i].distance[0] = 99999999;
        balls[i].distance[1] = -1;
    }
	initgraph(640,400);
	BeginBatchDraw();
	while(1)
	{
		setcolor(YELLOW);
		setfillcolor(GREEN);
		//绘制所有小球
		for(int i=0;i<number;i++)
		{
			fillcircle(balls[i].ball_x,balls[i].ball_y,20);
		}
		get_distance(balls);
		knock(balls);
		Sleep(2);
		FlushBatchDraw();
		setcolor(BLACK);
		setfillcolor(BLACK);
		//改变所有小球坐标
		for(i=0;i<number;i++)
		{
			fillcircle(balls[i].ball_x,balls[i].ball_y,20);
			balls[i].ball_x=balls[i].ball_x+balls[i].ball_vx;
			balls[i].ball_y=balls[i].ball_y+balls[i].ball_vy;
			if(balls[i].ball_x<=20||balls[i].ball_x>=620)
				balls[i].ball_vx=-balls[i].ball_vx;
			if(balls[i].ball_y<=20||balls[i].ball_y>=380)
				balls[i].ball_vy=-balls[i].ball_vy;
		}
	}
	EndBatchDraw();
	closegraph();
	
	return 0;
}
int i,j;
void get_distance(struct Ball balls[number])
{
	// 求解所有小球两两之间的距离平方
	for (i=0;i<number;i++)
	{
		for (j=0;j<number;j++)
		{

			if (i!=j) // 自己和自己不需要比
			{
                int dist2;
                dist2 = (balls[i].ball_x - balls[j].ball_x)*(balls[i].ball_x - balls[j].ball_x)
						+(balls[i].ball_y - balls[j].ball_y)*(balls[i].ball_y - balls[j].ball_y);
                if (dist2<balls[i].distance[0])
                    {
                        balls[i].distance[0] = dist2;
                        balls[i].distance[1] = j;
                    }
             }
		}
	}
}

void knock(struct Ball balls[number])
{
	// 判断球之间是否碰撞
    for (i=0;i<number;i++)
    {
         if (balls[i].distance[0]<=4*(20*20)) // 最小距离小于阈值，发生碰撞
         {
              j = balls[i].distance[1];
              // 交换速度             
              int temp;
              temp = balls[i].ball_vx; balls[i].ball_vx = balls[j].ball_vx; balls[j].ball_vx = temp;
              temp = balls[i].ball_vy; balls[i].ball_vy = balls[j].ball_vy; balls[j].ball_vy = temp;

              balls[j].distance[0] = 99999999; // 避免交换两次速度，将两个小球重新赋值
              balls[j].distance[1] = -1;
			  
              balls[i].distance[0] = 99999999; 
              balls[i].distance[1] = -1;
          }
     }
}
